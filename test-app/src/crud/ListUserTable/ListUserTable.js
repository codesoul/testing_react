import React from 'react'
import Table from 'react-bootstrap/Table';

const ListUserTable = (props) => {

    return (
        <>
            <Table striped bordered hover variant="dark">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>User</th>
                        <th>User Type</th>
                        <th>Avatar Url</th>
                        <th>Events Url</th>
                        <th>Subscription Url</th>
                    </tr>
                </thead>


                {/* <tbody>
                    {data.map((table) => {
                        return (
                            <>
                                <tr>
                                    <td>1</td>
                                    <td>Mark</td>
                                    <td>Otto</td>
                                    <td>@mdo</td>
                                </tr>
                            </>
                        )
                    })}
                </tbody> */}
            </Table>
        </>
    )
}

export default ListUserTable