import React, {useEffect, useState} from 'react';
import axios from 'axios';


export const Create = () => {
    const [data,setData] = useState();
    const url = "https://dummy.restapiexample.com/api/v1/create"; 
    
  const getAllNotes = () => {
    axios.post(`${url}`).then((response) =>{
        setData(response)
        console.log(response);
        console.log(data)
    })
  }

  useEffect(()=>{
    getAllNotes();
  })

  return (
    <div>Hello world</div>
  )
}
