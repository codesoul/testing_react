import React, { useEffect, useState } from 'react';
import ListUserTable from './ListUserTable/ListUserTable';

export const Listing = () => {
  const [data, setData] = useState([]);

  useEffect(()=>{
    fetch("https://api.github.com/users")
    .then(response => response.json())
    .then(
      data1 => 
      setData(data1));
  })
  return (
    <ListUserTable data = {data}/>
    
  )
}
